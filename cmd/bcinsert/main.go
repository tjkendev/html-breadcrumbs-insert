package main

import (
	"flag"
	"os"

	bcinsert "gitlab.com/tjkendev/html-breadcrumbs-insert"

	"github.com/golang/glog"
)

func main() {
	var optHelp bool
	var optInputDirectory string
	var optConfigFile string
	var optOnlyRemove bool

	flag.BoolVar(&optHelp, "h", false, "print help message and exit")
	flag.StringVar(&optInputDirectory, "i", "", "path of an input directory including HTML files to inject breadcrumbs")
	flag.StringVar(&optConfigFile, "c", "", "path of a config file")
	flag.BoolVar(&optOnlyRemove, "r", false, "only remove breadcrumb already injected")
	flag.Parse()

	if optHelp {
		// show usage
		flag.Usage()
		return
	}

	// check parameters

	if len(optInputDirectory) == 0 {
		glog.Error("specified no input directory path in -i option")
		os.Exit(1)
	}

	if len(optConfigFile) == 0 {
		glog.Error("specified no config file path in -c option")
		os.Exit(1)
	}

	config, err := bcinsert.LoadBreadcrumbConfig(optConfigFile)
	if err != nil {
		glog.Errorf("An error has occurred in loading config: %v", err)
		os.Exit(1)
	}

	rootDirNode, err := bcinsert.GetHTMLFiles(optInputDirectory, config.HTMLExtension)
	if err != nil {
		glog.Errorf("An error has occurred in loading html files: %v", err)
		os.Exit(1)
	}

	config.OnlyRemove = optOnlyRemove

	if err := bcinsert.InsertBreadcrumbs(config, rootDirNode); err != nil {
		glog.Fatal(err)
	}
}
