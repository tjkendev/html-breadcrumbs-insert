package html_breadcrumb_insert

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"reflect"
	"strings"

	yaml "gopkg.in/yaml.v2"
)

// DirNode is node struct for representing directory structure
type DirNode struct {
	PathName string
	NameMap  map[string]struct{}
	DirMap   map[string]*DirNode
}

func visitDirectory(directory string, checkFile func(string) bool) (*DirNode, error) {
	files, err := ioutil.ReadDir(directory)
	if err != nil {
		return nil, err
	}

	result := DirNode{
		PathName: directory,
		NameMap:  map[string]struct{}{},
		DirMap:   map[string]*DirNode{},
	}
	for _, file := range files {
		if file.IsDir() {
			cNode, err := visitDirectory(filepath.Join(directory, file.Name()), checkFile)
			if err != nil {
				return nil, err
			}
			result.DirMap[file.Name()] = cNode
			continue
		}
		if checkFile(file.Name()) {
			// found and add HTML file to result
			result.NameMap[file.Name()] = struct{}{}
		}
	}
	return &result, nil
}

// GetHTMLFiles gets all HTML files in a specified directory recursively
func GetHTMLFiles(directory, extension string) (*DirNode, error) {
	return visitDirectory(directory, func(fileName string) bool {
		return strings.HasSuffix(fileName, extension)
	})
}

// loadData is used while unmashalling a YAML file
func (n *BreadcrumbNode) loadData(data map[string]interface{}) error {
	for key, value := range data {
		switch v := value.(type) {
		case string:
			// load file name as string
			if strings.HasPrefix(key, "__") {
				hiddenKey := key[2:]
				if hiddenKey == DefaultIndexName {
					n.IndexName = sanitize(v)
					n.HiddenIndex = true
				}
			} else {
				n.NameMap[key] = sanitize(v)
			}

		case map[interface{}]interface{}:
			// convert map[interface{}]interface{} to map[string]interface{}
			converted := map[string]interface{}{}
			for cKey, cValue := range v {
				if cName, ok := cKey.(string); ok {
					// string type: set value directly
					converted[cName] = cValue
				} else {
					// other type: set value after converting value to string
					converted[fmt.Sprintf("%v", cKey)] = cValue
				}
			}

			// load data recursively
			subNode := BreadcrumbNode{
				IndexName: key,
				NameMap:   map[string]string{},
				DirMap:    map[string]*BreadcrumbNode{},
			}
			if err := subNode.loadData(converted); err != nil {
				return err
			}
			n.DirMap[key] = &subNode

		default:
			// failed to load data due to invalid type
			return fmt.Errorf("Parse Error: Value = \"%v\", Type = %v", v, reflect.TypeOf(v))
		}
	}

	if defaultName, ok := n.NameMap[DefaultIndexName]; ok {
		n.IndexName = sanitize(defaultName)
	}
	return nil
}

// UnmarshalYAML unmarshals a YAML Data to load a breadcrumb config.
func (n *BreadcrumbNode) UnmarshalYAML(unmarshal func(interface{}) error) error {
	data := map[string]interface{}{}
	if err := unmarshal(&data); err != nil {
		return err
	}
	if err := n.loadData(data); err != nil {
		return err
	}
	return nil
}

// LoadBreadcrumbConfig loads a breadcrumb config from a specified YAML file.
func LoadBreadcrumbConfig(yamlFileName string) (*BreadcrumbConfig, error) {
	buf, err := ioutil.ReadFile(yamlFileName)
	if err != nil {
		return nil, err
	}

	root := BreadcrumbNode{
		IndexName: DefaultHomeName,
		NameMap:   map[string]string{},
		DirMap:    map[string]*BreadcrumbNode{},
	}
	config := BreadcrumbConfig{
		DefaultIndex:          DefaultIndexPage,
		TagName:               DefaultTagName,
		Separator:             DefaultSeparator,
		HTMLExtension:         DefaultHTMLExtension,
		TargetSelector:        DefaultTargetSelector,
		BreadcrumbRootID:      DefaultBreadcrumbID,
		BreadcrumbLinkClass:   DefaultBreadcrumbLinkClass,
		BreadcrumbNoLinkClass: DefaultBreadcrumbNoLinkClass,
		BreadcrumbStruct:      root,
	}

	if err := yaml.Unmarshal(buf, &config); err != nil {
		return nil, err
	}

	// sanitize data
	config.DefaultIndex = sanitize(config.DefaultIndex)
	config.HTMLExtension = sanitize(config.HTMLExtension)
	config.TagName = sanitize(config.TagName)
	config.Separator = sanitize(config.Separator)
	config.BreadcrumbRootID = sanitize(config.BreadcrumbRootID)
	config.BreadcrumbLinkClass = sanitize(config.BreadcrumbLinkClass)
	config.BreadcrumbNoLinkClass = sanitize(config.BreadcrumbNoLinkClass)
	config.TargetSelector = sanitize(config.TargetSelector)

	return &config, nil
}
