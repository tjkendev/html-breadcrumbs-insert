# HTML Breadcrumbs Insert Tool

This tool inserts breadcrumbs into HTML files directly.

## Install

This tool is implemented in Golang.

### Install using `go install`

```sh
$ go install gitlab.com/tjkendev/html-breadcrumbs-insert/cmd/bcinsert@latest
```

### Install from this source code

```sh
$ make install
```

## Usage

Insert breadcrumbs in the target directory recursively

```sh
$ bcinsert -i <target-directory> -c <breadcrumb-config (YAML file)>
```

To remove breadcrumbs after insertion, use `-r` option.

```sh
$ bcinsert -i <target-directory> -c <breadcrumb-config (YAML file)> -r
```

### Example

```
$ tree .
.
├── breadcrumb.yml
└── public
    ├── A1
    │   └── A2
    │       └── test.html
    ├── C1
    │   ├── C2
    │   │   └── test.html
    │   └── index.html
    ├── index.html
    └── sample.html

5 directories, 6 files

$ cat breadcrumb.yml
# "TargetSelector": a selector to append breadcrumbs in HTML files (In this example, breadcrumbs is inserted into the tag with "header" id attribute)
TargetSelector: "#header"
# "DefaultIndex": a default index page in parent directories
DefaultIndex: "index.html"
# "HTMLExtension": the file extension of HTML files
HTMLExtension: "html"
# "TagName": a tag name of breadcrumb's root element
TagName: "div"
# "BreadcrumbRootID": an id attribute's name contained in the breadcrumb's root element
BreadcrumbRootID: "breadcrumb"
# "BreadcrumbLinkClass": a class name contained in 'a' tags that is linked to the corresponding page in breadcrumbs
BreadcrumbLinkClass: "breadcrumb-link"
# "BreadcrumbNoLinkClass": a class name contained in 'span' tags that has no link in breadcrumbs
BreadcrumbNoLinkClass: "breadcrumb-text"
# "Separator": the breadcrumbs' hierarchy separator
Separator: '>'
# "BreadcrumbStrcut": directories information to insert breadcrumbs
BreadcrumbStruct:
  index: "home"
  A1:
    # "__index": hidden index name (no link in breadcrumbs)
    __index: "A"
    A2:
      test: "test1"
  sample: "sample"
  C1:
    index: "C"
    C2:
      test: "test2"

$ bcinsert -i ./public -c ./breadcrumb.yml
```

In `./A1/A2/test.html`, for example, the following HTML text is inserted into the tag with `header` id attribute.

```html
<div id="breadcrumb"><a href="../../index.html" class="breadcrumb-link">home</a> &gt; <span class="breadcrumb-text">A</span> &gt; <a href="./test.html" class="breadcrumb-link">test1</a></div></div>
```

and the breadcrumb is displayed as below. (`home` and `test1` is linked to the corresponding page)

```
home > A > test1
```
