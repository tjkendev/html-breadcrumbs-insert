package html_breadcrumb_insert

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/golang/glog"
)

// EntireConfig is an injection config struct which is referred when injecting breadcrumbs
type EntireConfig struct {
	OnlyRemove            bool
	IndexPage             string
	Extension             string
	SelectorName          string
	BreadcrumbID          string
	BreadcrumbLinkClass   string
	BreadcrumbNoLinkClass string
	TagName               string
	Separator             string
}

// PageInfo is a page info of each page showed in breadcrumbs.
type PageInfo struct {
	Level    int
	Name     string
	FileName string
	NoLink   bool
}

// generateLink generates HTML link which showed in breadcrumbs.
func generateLink(info *PageInfo, curLevel int, config EntireConfig) string {
	if info == nil {
		return ""
	}

	path := info.FileName
	levelDiff := curLevel - info.Level
	if levelDiff > 0 {
		path = fmt.Sprintf("%s%s", strings.Repeat("../", levelDiff), path)
	} else {
		path = fmt.Sprintf("./%s", path)
	}
	if info.NoLink {
		return fmt.Sprintf(`<span class="%s">%s</span>`, config.BreadcrumbNoLinkClass, info.Name)
	}
	return fmt.Sprintf(`<a href="%s" class="%s">%s</a>`, path, config.BreadcrumbLinkClass, info.Name)
}

// removeBreadcrumbFromHTML removes breadcrumbs added in HTML.
func removeBreadcrumbFromHTML(doc *goquery.Document, entireConfig EntireConfig) error {
	// Check if breadcrumbs already exists (if exists, remove them)
	doc.Find(fmt.Sprintf("#%s", entireConfig.BreadcrumbID)).Remove()
	return nil
}

// insertBreadcrumbToHTML inserts breadcrumbs into HTML.
func insertBreadcrumbIntoHTML(doc *goquery.Document, entireConfig EntireConfig, stack []*PageInfo, curLevel int) error {

	// Check if breadcrumbs already exists (if exists, remove them)
	doc.Find(fmt.Sprintf("#%s", entireConfig.BreadcrumbID)).Remove()

	// Create breadcrumb list
	breadcrumbHTML := bytes.NewBufferString(fmt.Sprintf(`<%s id="%s">`, entireConfig.TagName, entireConfig.BreadcrumbID))
	for i, info := range stack {
		if i > 0 {
			breadcrumbHTML.WriteString(fmt.Sprintf(" %s ", entireConfig.Separator))
		}
		breadcrumbHTML.WriteString(generateLink(info, curLevel, entireConfig))
	}
	breadcrumbHTML.WriteString(fmt.Sprintf(`</%s>`, entireConfig.TagName))

	// Append created html string
	doc.Find(entireConfig.SelectorName).AppendHtml(breadcrumbHTML.String())

	return nil
}

// updateHTML updates HTML files.
func updateHTML(filePath string, entireConfig EntireConfig, stack []*PageInfo, curLevel int) error {
	fInfo, err := os.Stat(filePath)
	if err != nil {
		return err
	}

	buf, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}
	doc, err := goquery.NewDocumentFromReader(bytes.NewBuffer(buf))
	if err != nil {
		return err
	}

	if entireConfig.OnlyRemove {
		// remove only
		if err := removeBreadcrumbFromHTML(doc, entireConfig); err != nil {
			return err
		}
	} else {
		// inject breadcrumbs
		if err := insertBreadcrumbIntoHTML(doc, entireConfig, stack, curLevel); err != nil {
			return err
		}
	}

	// Output as string and overwrite file
	result, err := doc.Html()
	if err != nil {
		return err
	}
	if err := ioutil.WriteFile(filePath, []byte(result), fInfo.Mode()); err != nil {
		return err
	}
	return nil
}

// visitBreadcrumbTarget is a visitor function to update HTML files recursively.
func visitBreadcrumbTarget(entireConfig EntireConfig, bNode *BreadcrumbNode, dNode *DirNode, stack []*PageInfo) error {
	if bNode == nil {
		return errors.New("breadcrumb node is nil")
	}
	if dNode == nil {
		return errors.New("directory node is nil")
	}

	for fileName, name := range bNode.NameMap {
		fullName := fmt.Sprintf("%s.%s", fileName, entireConfig.Extension)

		// check if the file exists
		if _, ok := dNode.NameMap[fullName]; !ok {
			glog.Errorf("An error has occurred while updating: file \"%s\" does not exists %v", fullName, stack)
			continue
		}
		curLevel := len(stack)

		// push current file
		stack = append(stack, &PageInfo{
			Level:    curLevel,
			Name:     name,
			NoLink:   bNode.HiddenIndex,
			FileName: fullName,
		})

		// update HTML file (insert or remove breadcrumbs)
		filePath := filepath.Join(dNode.PathName, fullName)
		if err := updateHTML(filePath, entireConfig, stack, curLevel); err != nil {
			glog.Errorf("An error has occurred while updating %s: %s", filePath, err)
		} else {
			glog.Infof("Updated Successfully: %s", filePath)
		}

		// pop current file
		stack = stack[:len(stack)-1]
	}

	for dirName, bSubNode := range bNode.DirMap {
		dSubNode, ok := dNode.DirMap[dirName]
		if !ok {
			glog.Errorf("An error has occurred while updating: directory \"%s\" does not exists", dirName)
			continue
		}
		// push current directory info
		stack = append(stack, &PageInfo{
			Level:    len(stack),
			Name:     bNode.IndexName,
			NoLink:   bNode.HiddenIndex,
			FileName: entireConfig.IndexPage,
		})
		if err := visitBreadcrumbTarget(entireConfig, bSubNode, dSubNode, stack); err != nil {
			return err
		}
		// pop current directory info
		stack = stack[:len(stack)-1]
	}
	return nil
}

// InsertBreadcrumbs inserts breadcrumbs into HTML files with specified config and directories.
func InsertBreadcrumbs(config *BreadcrumbConfig, rootDirNode *DirNode) error {
	if config == nil {
		return errors.New("config is nil")
	}
	if rootDirNode == nil {
		return errors.New("rootDirNode is nil")
	}

	stack := []*PageInfo{}

	entireConfig := EntireConfig{
		OnlyRemove:            config.OnlyRemove,
		IndexPage:             config.DefaultIndex,
		SelectorName:          config.TargetSelector,
		Extension:             config.HTMLExtension,
		Separator:             config.Separator,
		TagName:               config.TagName,
		BreadcrumbID:          config.BreadcrumbRootID,
		BreadcrumbLinkClass:   config.BreadcrumbLinkClass,
		BreadcrumbNoLinkClass: config.BreadcrumbNoLinkClass,
	}
	rootBreadcrumbNode := config.BreadcrumbStruct
	if err := visitBreadcrumbTarget(entireConfig, &rootBreadcrumbNode, rootDirNode, stack); err != nil {
		return err
	}
	return nil
}
