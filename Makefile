NAME := bcinsert
DST := bin

install:
	go install ./cmd/$(NAME)

build:
	go build -o ./$(DST)/$(NAME)

clean:
	rm -rf ./$(DST)
