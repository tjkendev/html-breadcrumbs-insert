package html_breadcrumb_insert

const (
	// DefaultIndexPage is default index filename of parent directories.
	DefaultIndexPage = "index.html"

	// DefaultHomeName is default breadcrumb name of root directory.
	DefaultHomeName = "home"
	// DefaultIndexName is default breadcrumb name of index page.
	DefaultIndexName = "index"
	// DefaultHTMLExtension is default extension of HTML files.
	DefaultHTMLExtension = "html"

	// DefaultTargetSelector is default selector for specifying where you want to insert breadcrumbs in html.
	DefaultTargetSelector = "#header"
	// DefaultTagName is a default tag name of breadcrumb's root element in html.
	DefaultTagName = "div"
	// DefaultBreadcrumbID is default id attribute's value contained in breadcrumb's root element in html. (default: id="breadcrumb")
	DefaultBreadcrumbID = "breadcrumb"
	// DefaultBreadcrumbLinkClass is default class names of breadcrumb which is linked to some page.
	DefaultBreadcrumbLinkClass = "breadcrumb-link"
	// DefaultBreadcrumbNoLinkClass is default class names of breadcrumb which is not linked to any page.
	DefaultBreadcrumbNoLinkClass = "breadcrumb-text"
	// DefaultSeparator is default hierarchy separator of breadcrumbs. (default: ">")
	DefaultSeparator = "&gt;"
)

// BreadcrumbConfig is configuration struct for this tool.
type BreadcrumbConfig struct {
	// BreadcrumbStruct is a tree structure which have infomation to insert breadcrumbs.
	BreadcrumbStruct BreadcrumbNode `yaml:"BreadcrumbStruct"`

	// OnlyRemove is an option. If this value is true, this tool only remove breadcrumbs already inserted.
	OnlyRemove bool `yaml:"-"`

	// DefaultIndex is a name of default index pages. (default "index.html")
	DefaultIndex string `yaml:"DefaultIndex"`

	// HTMLExtension is a file extension of HTML files.
	HTMLExtension string `yaml:"HTMLExtension"`

	// TagName is a tag name of breadcrumb's root element.
	TagName string `yaml:"TagName"`

	// BreadcrumbRootID is an id attribute's value contained in breadcrumb's root element.
	BreadcrumbRootID string `yaml:"BreadcrumbRootID"`

	// BreadcrumbLinkClass is class names contained in 'a' tags which is linked to a page in breadcrumbs.
	BreadcrumbLinkClass string `yaml:"BreadcrumbLinkClass"`

	// BreadcrumbNoLinkClass is class names contained in 'span' tags which is not linked to a page in breadcrumbs.
	BreadcrumbNoLinkClass string `yaml:"BreadcrumbNoLinkClass"`

	// Separator is a breadcrumb hierarchy separator.
	Separator string `yaml:"Separator"`

	// TargetSelector is a target selector for inserting breadcrumb.
	TargetSelector string `yaml:"TargetSelector"`
}

// BreadcrumbNode is node struct for representing breadcrumb info.
type BreadcrumbNode struct {
	IndexName   string
	HiddenIndex bool
	NameMap     map[string]string
	DirMap      map[string]*BreadcrumbNode
}
