module gitlab.com/tjkendev/html-breadcrumbs-insert

go 1.18

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/golang/glog v1.0.0
	github.com/microcosm-cc/bluemonday v1.0.19
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/gorilla/css v1.0.0 // indirect
	golang.org/x/net v0.0.0-20210916014120-12bc252f5db8 // indirect
)
