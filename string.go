package html_breadcrumb_insert

import (
	"github.com/microcosm-cc/bluemonday"
)

func sanitize(str string) string {
	return bluemonday.StrictPolicy().Sanitize(str)
}
